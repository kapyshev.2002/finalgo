package endpoint

import (
	"context"
	endpoint "github.com/go-kit/kit/endpoint"
	service "github.com/plutov/packagemain/13-go-kit-2/bugs/pkg/service"
)

type CreateRequest struct {
	Bug string `json:"bug"`
}

type CreateResponse struct {
	E0 error `json:"e0"`
}

func MakeCreateEndpoint(s service.BugsService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest)
		e0 := s.Create(ctx, req.Bug)
		return CreateResponse{E0: e0}, nil
	}
}

func (r CreateResponse) Failed() error {
	return r.E0
}

type Failure interface {
	Failed() error
}

func (e Endpoints) Create(ctx context.Context, bug string) (e0 error) {
	request := CreateRequest{Bug: bug}
	response, err := e.CreateEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(CreateResponse).E0
}

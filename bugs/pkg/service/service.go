package service

import "context"

type BugsService interface {
	Create(ctx context.Context, bug string) error
}

type basicBugsService struct{}

func (b *basicBugsService) Create(ctx context.Context, bug string) (e0 error) {
	return e0
}

func NewBasicBugsService() BugsService {
	return &basicBugsService{}
}

func New(middleware []Middleware) BugsService {
	var svc BugsService = NewBasicBugsService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

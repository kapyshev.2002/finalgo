package http

import (
	http "github.com/go-kit/kit/transport/http"
	endpoint "github.com/plutov/packagemain/13-go-kit-2/users/pkg/endpoint"
	http1 "net/http"
)

func NewHTTPHandler(endpoints endpoint.Endpoints, options map[string][]http.ServerOption) http1.Handler {
	m := http1.NewServeMux()
	makeCreateHandler(m, endpoints, options["Create"])
	return m
}

package endpoint

import (
	endpoint "github.com/go-kit/kit/endpoint"
	service "github.com/plutov/packagemain/13-go-kit-2/users/pkg/service"
)

type Endpoints struct {
	CreateEndpoint endpoint.Endpoint
}

func New(s service.UsersService, mdw map[string][]endpoint.Middleware) Endpoints {
	eps := Endpoints{CreateEndpoint: MakeCreateEndpoint(s)}
	for _, m := range mdw["Create"] {
		eps.CreateEndpoint = m(eps.CreateEndpoint)
	}
	return eps
}

package endpoint

import (
	"context"
	endpoint "github.com/go-kit/kit/endpoint"
	service "github.com/plutov/packagemain/13-go-kit-2/users/pkg/service"
)

type CreateRequest struct {
	Email string `json:"email"`
}

type CreateResponse struct {
	E0 error `json:"e0"`
}

func MakeCreateEndpoint(s service.UsersService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest)
		e0 := s.Create(ctx, req.Email)
		return CreateResponse{E0: e0}, nil
	}
}

func (r CreateResponse) Failed() error {
	return r.E0
}

type Failure interface {
	Failed() error
}

func (e Endpoints) Create(ctx context.Context, email string) (e0 error) {
	request := CreateRequest{Email: email}
	response, err := e.CreateEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(CreateResponse).E0
}

package endpoint

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	service "github.com/plutov/packagemain/13-go-kit-2/notificator/pkg/service"
)

type SendEmailRequest struct {
	Email   string `json:"email"`
	Content string `json:"content"`
}

type SendEmailResponse struct {
	Id string
	E0 error `json:"e0"`
}

func MakeSendEmailEndpoint(s service.NotificatorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SendEmailRequest)
		id, e0 := s.SendEmail(ctx, req.Email, req.Content)
		return SendEmailResponse{Id: id, E0: e0}, nil
	}
}

func (r SendEmailResponse) Failed() error {
	return r.E0
}

type Failure interface {
	Failed() error
}

func (e Endpoints) SendEmail(ctx context.Context, email string, content string) (e0 error) {
	request := SendEmailRequest{
		Content: content,
		Email:   email,
	}
	response, err := e.SendEmailEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(SendEmailResponse).E0
}

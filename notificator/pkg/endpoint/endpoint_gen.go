package endpoint

import (
	endpoint "github.com/go-kit/kit/endpoint"
	service "github.com/plutov/packagemain/13-go-kit-2/notificator/pkg/service"
)

type Endpoints struct {
	SendEmailEndpoint endpoint.Endpoint
}

func New(s service.NotificatorService, mdw map[string][]endpoint.Middleware) Endpoints {
	eps := Endpoints{SendEmailEndpoint: MakeSendEmailEndpoint(s)}
	for _, m := range mdw["SendEmail"] {
		eps.SendEmailEndpoint = m(eps.SendEmailEndpoint)
	}
	return eps
}

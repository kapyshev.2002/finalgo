package service

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type NotificatorService interface {
	SendEmail(ctx context.Context, email string, content string) (string, error)
}

type basicNotificatorService struct{}

func (b *basicNotificatorService) SendEmail(ctx context.Context, email string, content string) (string, error) {
	id := uuid.NewV4()
	return id.String(), nil
}

func NewBasicNotificatorService() NotificatorService {
	return &basicNotificatorService{}
}

func New(middleware []Middleware) NotificatorService {
	var svc NotificatorService = NewBasicNotificatorService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

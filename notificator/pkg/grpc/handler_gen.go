package grpc

import (
	grpc "github.com/go-kit/kit/transport/grpc"
	endpoint "github.com/plutov/packagemain/13-go-kit-2/notificator/pkg/endpoint"
	pb "github.com/plutov/packagemain/13-go-kit-2/notificator/pkg/grpc/pb"
)

type grpcServer struct {
	sendEmail grpc.Handler
}

func NewGRPCServer(endpoints endpoint.Endpoints, options map[string][]grpc.ServerOption) pb.NotificatorServer {
	return &grpcServer{sendEmail: makeSendEmailHandler(endpoints, options["SendEmail"])}
}
